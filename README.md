# JCC Rocket League Overlay

A simple/clean Rocket League Overlay for e-sports. Based on a combination of the default overlay and Dazerin's Codename:covert overlay.
Designed to be used with OBS.

Dependencies:

Rocket League (duh)

BakkesMod (https://bakkesmod.com/download.php)

SOS plugin for BakkesMod (https://gitlab.com/bakkesplugins/sos)
    

