var BLUE_SERIES_SCORE = 0;
var ORANGE_SERIES_SCORE = 0;
var MAX_SERIES_SCORE = 3;

//fill data vars from sos-data-grab with garbage values for testing
function fill() {
  players = [new Player("null", 0, 0, 0, 0, 0, 0),
          new Player("null", 0, 0, 0, 0, 0, 0),
          new Player("null", 0, 0, 0, 0, 0, 0),
          new Player("null", 1, 0, 0, 0, 0, 0),
          new Player("null", 1, 0, 0, 0, 0, 0),
          new Player("null", 1, 0, 0, 0, 0, 0)];
  teams = [new Team("BLUE", "#0000ff", "#0000ff", 0), new Team("ORANGE", "#ff0000", "#ff0000", 0)];
  clockDisplay = "5:00";
}

function setSeriesScore(blueScore, orangeScore, numToWin) {
  var blueScoreBox = document.getElementById("blue-ss");
  var orangeScoreBox = document.getElementById("orange-ss");

  function removeChildren(parent) {
    while (true) {
      try {
        parent.removeChild(parent.firstChild);
      } catch(e) {
        break;
      }
    }
  }
  function addTicker(parent, color) {
    var node = document.createElement("DIV");
    if(color == 0) {
      //blue team
      node.className = "ticker blue";
    } else if(color == 1) {
      //orange team
      node.className = "ticker orange";
    } else {
      //empty
      node.className = "ticker empty";
    }
    parent.appendChild(node);
  }

  removeChildren(blueScoreBox);
  removeChildren(orangeScoreBox);

  for(let i = 0; i < numToWin; i++) {
    //blue team ticks
    if(i < blueScore) {
      addTicker(blueScoreBox, 0);
    } else {
      addTicker(blueScoreBox, -1);
    }

    //orange team ticks
    if(i < orangeScore) {
      addTicker(orangeScoreBox, 1);
    } else {
      addTicker(orangeScoreBox, -1);
    }
  }
}

function getCssVar(name) {
  return getComputedStyle(document.documentElement,null).getPropertyValue(name);
}
function setCssVar(name, value) {
  document.documentElement.style.setProperty(name, value);
}

function centerOverlay() {
  var timeWidth = parseInt(getCssVar("--scorebar-time-width"));
  var dataWidth = parseInt(getCssVar("--scorebar-data-width"));
  document.getElementById("scorebar").style.marginLeft = (window.innerWidth - timeWidth - dataWidth * 2 - 150) / 2;
}

function setBoostMeter(value, team) {
  //set number in center
  document.getElementById("boost-text").innerHTML = value;
  //draw circle
  var c = document.getElementById("boost-circle")
  var ctx = c.getContext("2d");
  var cSize = parseInt(getCssVar("--boost-meter-size"));
  c.width = cSize;
  c.height = cSize;
  ctx.lineWidth = 15;
  ctx.lineJoin = "round";
  ctx.lineCap = "round";

  //inner shading
  ctx.fillStyle = "rgba(0, 0, 0, 0.5)";
  ctx.beginPath();
  ctx.arc(cSize / 2, cSize / 2, cSize / 2, 0, 2*Math.PI);
  ctx.fill();
  //outer shading

  ctx.beginPath();
  ctx.arc(cSize / 2, cSize / 2, cSize / 2, -Math.PI / 2, (Math.PI / -50) * 100 - Math.PI / 2, true);
  ctx.arc(cSize / 2, cSize / 2, cSize / 3 - 10, (Math.PI / -50) * 100 - Math.PI / 2, -Math.PI / 2);
  ctx.lineTo(cSize / 2, 0);
  ctx.fill();

  ctx.beginPath();
  ctx.strokeStyle = ctx.fillStyle;
  //colored boost circle
  if (team == 0) {
    ctx.fillStyle = getCssVar("--blue-color");
  } else {
    ctx.fillStyle = getCssVar("--orange-color");
  }
  ctx.strokeStyle = ctx.fillStyle;
  ctx.beginPath();
  ctx.arc(cSize / 2, cSize / 2, cSize / 2 - 5, -Math.PI / 2, (Math.PI / -50) * value - Math.PI / 2, true);
  ctx.arc(cSize / 2, cSize / 2, cSize / 3 - 5, (Math.PI / -50) * value - Math.PI / 2, -Math.PI / 2);
  ctx.lineTo(cSize / 2, 5);

  ctx.stroke();
  ctx.fill();
}

function hidePlayerStats() {
  var boostMeter = document.getElementById("boost-meter");
  var statMain = document.getElementById("player-stats");
  var statEndcap = document.getElementById("stat-endcap");

  boostMeter.className = "boost-transition";
  statMain.className = "stat-transition";
  statEndcap.className = "stat-transition";
}
function showPlayerStats() {
  var boostMeter = document.getElementById("boost-meter");
  var statMain = document.getElementById("player-stats");
  var statEndcap = document.getElementById("stat-endcap");

  boostMeter.className = "";
  statMain.className = "";
  statEndcap.className = "";
}
function hideScoreBar() {
  var scoreBar = document.getElementById("scorebar");
  scoreBar.className = "scorebar-transition";
}
function showScoreBar() {
  var scoreBar = document.getElementById("scorebar");
  scoreBar.className = "";
}
function updateTime(string) {
  document.getElementById("time").innerHTML = string;
}
function updateTeamNames(blueName, orangeName) {
  document.getElementById("blue-name").innerHTML = blueName;
  document.getElementById("orange-name").innerHTML = orangeName;
}
function updateMatchScore(blueScore, orangeScore) {
  document.getElementById("blue-score").innerHTML = blueScore;
  document.getElementById("orange-score").innerHTML = orangeScore;
}
function updateTeamColors(blueColor, orangeColor) {
  setCssVar("--blue-color", blueColor);
  setCssVar("--orange-color", orangeColor);
  //need to refresh boost meter after this.
}
function updatePlayerStats(name, goals, assists, saves, shots) {
  document.getElementById("stat-goals").innerHTML = goals;
  document.getElementById("stat-assists").innerHTML = assists;
  document.getElementById("stat-saves").innerHTML = saves;
  document.getElementById("stat-shots").innerHTML = shots;

  document.getElementById("playername").innerHTML = name;
}
function setSidebarColor(team) {
  if (team == 0) {
    document.getElementById("stat-endcap").style.backgroundColor = getCssVar("--blue-color");
  } else {
    document.getElementById("stat-endcap").style.backgroundColor = getCssVar("--orange-color");
  }
}

function onload() {
  fill();
  centerOverlay();
  hidePlayerStats();
  hideScoreBar();
}

function updateUI() {
  updateTeamColors("#" + teams[0].colorPrimary, "#" + teams[1].colorPrimary);
  updateMatchScore(teams[0].score, teams[1].score);
  updateTeamNames(teams[0].name, teams[1].name);
  updateTime(clockDisplay);
  updatePlayerStats(currentPlayer.name, currentPlayer.goals, currentPlayer.assists, currentPlayer.saves, currentPlayer.shots);
  setBoostMeter(currentPlayer.boost, currentPlayer.team);
  setSidebarColor(currentPlayer.team);
  setSeriesScore(BLUE_SERIES_SCORE, ORANGE_SERIES_SCORE, MAX_SERIES_SCORE);
  if (scorebarShouldDisplay) {
    showScoreBar();
  } else {
    hideScoreBar();
  }
  if (playerDataShouldDisplay) {
    showPlayerStats();
  } else {
    hidePlayerStats();
  }
}
