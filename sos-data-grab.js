class Player {
  constructor(name, team, goals, assists, saves, shots, boost) {
    this.name = name;
    this.team = team;
    this.goals = goals;
    this.assists = assists;
    this.saves = saves;
    this.shots = shots;
    this.boost = boost;
  }
}
class Team {
  constructor(name, colorPrimary, score) {
    this.name = name;
    this.score = score;
    this.colorPrimary = colorPrimary;
  }
}

var currentPlayer = new Player(0, 0, 0, 0, 0, 0);
var teams = [];
var clockDisplay;
var scorebarShouldDisplay = true;
var playerDataShouldDisplay = false;


window.onload = function() {
  var webSocket = new WebSocket("ws://localhost:49122/");
  webSocket.onmessage = function(e) {
    var message = JSON.parse(e.data);
    if(message.event == "game:update_state") {
      handleStateUpdate(message.data);
    } else if (message.event == 'game:pre_countdown_begin'){
      scorebarShouldDisplay = true;
    } else if (message.event == "game:match_ended") {
      scorebarShouldDisplay = true;
      if (message.data.winner_team_num == 0) {
        BLUE_SERIES_SCORE++;
      } else {
        ORANGE_SERIES_SCORE++;
      }
    }
  }
  onload();
}

function handleStateUpdate(data) {


  playerDataShouldDisplay = true;
  //extract player data from websocket event.
  try {
    currentPlayer = new Player(data.players[data.game.target].name, data.players[data.game.target].team, data.players[data.game.target].goals, data.players[data.game.target].assists, data.players[data.game.target].saves, data.players[data.game.target].shots, data.players[data.game.target].boost);
  } catch (e) {
    playerDataShouldDisplay = false;
  }

  //extract team data from websocket event.
  teams = [new Team(data.game.teams[0].name, data.game.teams[0].color_primary, data.game.teams[0].score),
    new Team(data.game.teams[1].name, data.game.teams[1].color_primary, data.game.teams[1].score)
  ];

  //Extract time data from websocket event.
  clockDisplay = (data.game.isOT ? "+" : "") + Math.floor(data.game.time_seconds / 60) + ":" + ("" + (data.game.time_seconds % 60)).padStart(2, "0");

  updateUI();
}
